package api.norma;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/norma")
public class RestNorma {
	
	private static final List<Norma> normas = new ArrayList<>();
	static{
		normas.add(new Norma(1,"ABNT NBR ISO 9000","Norma responsável por definir os requisitos para a implementação de um sistema de gestão de qualidade",CategoriaNorma.GESTAO_QUALIDADE, LocalidadeNorma.NACIONAL));
		normas.add(new Norma(2,"ISO / TS 16949","Norma responsável por definir qualidade nos processos de empresas do ramo de indústria automotiva",CategoriaNorma.QUALIDADE_INDUSTRIA_AUTOMOTIVA,LocalidadeNorma.INTERNACIONAL));
	}
	
	@GET
	@Path("/consultar/nacionais")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Norma> consultarNormasNacionais(){
		return normas.stream().filter(n -> LocalidadeNorma.NACIONAL == n.getLocalidadeNorma()).collect(Collectors.toList());
	}

	@GET
	@Path("/consultar/internacionais")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Norma> consultarNormasInternacionais(){
		return normas.stream().filter(n -> LocalidadeNorma.INTERNACIONAL == n.getLocalidadeNorma()).collect(Collectors.toList());
	}
	
}
