package api.norma;

public enum LocalidadeNorma {
	
	INTERNACIONAL("Internacional"),NACIONAL("Nacional");
	private String descricao;
	
	private LocalidadeNorma(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
	
}
