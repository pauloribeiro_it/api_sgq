package api.norma;

public enum CategoriaNorma {
	GESTAO_QUALIDADE("Gest�o de qualidade"), QUALIDADE_INDUSTRIA_AUTOMOTIVA("Qualidade na Ind�stria Automotiva");
	
	private String descricao;
	
	private CategoriaNorma(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
}
