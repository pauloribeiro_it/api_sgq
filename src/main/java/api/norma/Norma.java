package api.norma;

import org.codehaus.jackson.annotate.JsonIgnore;

public class Norma {
	
	private Integer id;
	private String nome;
	private String descricao;
	private CategoriaNorma categoria;
	private LocalidadeNorma localidadeNorma;
	
	public Norma() {
		
	}
	
	public Norma(Integer id, String nome, String descricao, CategoriaNorma categoria, LocalidadeNorma localidade) {
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.categoria = categoria;
		this.localidadeNorma = localidade;
	}

	@JsonIgnore
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public CategoriaNorma getCategoria() {
		return categoria;
	}
	public void setCategoria(CategoriaNorma categoria) {
		this.categoria = categoria;
	}
	
	@JsonIgnore
	public LocalidadeNorma getLocalidadeNorma() {
		return localidadeNorma;
	}

	public void setLocalidadeNorma(LocalidadeNorma localidadeNorma) {
		this.localidadeNorma = localidadeNorma;
	}

	
	
}
