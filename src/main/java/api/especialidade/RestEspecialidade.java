package api.especialidade;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/especialidade")
public class RestEspecialidade {
	private static final List<Especialidade> especialidades = new ArrayList<>();
	static{
		especialidades.add(new Especialidade(1, "Accenture", "Empresa de consultoria para empresas do ramo automobilístico", "55 (11) 1234-5678", "01234567000000",
				"contato@accenture.com", TipoEspecialidade.CONSULTORIA));
		especialidades.add(new Especialidade(2, "QualiMaster", "Empresa de assessoria para empresas do ramo automobilístico", "55 (11) 1222-5678", "01111111000000",
				"contato@qualimaster.com", TipoEspecialidade.ASSESSORIA));
	}
	
	@GET
	@Path("/assessoria/consultar")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Especialidade> consultarAssessorias(){
		return especialidades.stream().filter(e ->TipoEspecialidade.ASSESSORIA == e.getEspecialidade()).collect(Collectors.toList());
	}
	
	@GET
	@Path("/consultoria/consultar")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Especialidade> consultarConsultorias(){
		return especialidades.stream().filter(e ->TipoEspecialidade.CONSULTORIA == e.getEspecialidade()).collect(Collectors.toList());
	}
	
	@POST
	@Path("/consultoria/contratar/{cnpj}")
	public Response contratarConsultoria(@PathParam(value = "cnpj") String cnpj) {
		return Response.ok("Consultoria contratada com sucesso.").build();
	}
	
	@POST
	@Path("/assessoria/contratar/{cnpj}")
	public Response contratarAssessoria(@PathParam(value = "cnpj") String cnpj) {
		return Response.ok("Assessoria contratada com sucesso.").build();
	}
}
