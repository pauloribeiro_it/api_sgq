package api.especialidade;

import org.codehaus.jackson.annotate.JsonIgnore;

public class Especialidade {
	
	@JsonIgnore
	private Integer id;
	private String nome;
	private String descricao;
	private String telefone;
	private String cnpj;
	private String email;
	@JsonIgnore
	private TipoEspecialidade especialidade;
	
	public Especialidade() {
		
	}
	
	public Especialidade(Integer id, String nome, String descricao, String telefone, String cnpj, String email,
			TipoEspecialidade especialidade) {
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.telefone = telefone;
		this.cnpj = cnpj;
		this.email = email;
		this.especialidade = especialidade;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public TipoEspecialidade getEspecialidade() {
		return especialidade;
	}
	public void setEspecialidade(TipoEspecialidade especialidade) {
		this.especialidade = especialidade;
	}

}
